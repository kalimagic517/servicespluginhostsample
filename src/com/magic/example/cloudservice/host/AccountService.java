package com.magic.example.cloudservice.host;

import java.util.List;

import com.htc.cloudservice.common.AccountChangedCallback;
import com.htc.cloudservice.common.ServiceAuthenticator;

import android.accounts.AccountManager;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class AccountService extends IntentService {

	public AccountService() {
		super("");
	}

	public AccountService(String name) {
		super(name);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (AccountManager.LOGIN_ACCOUNTS_CHANGED_ACTION.equals(intent
				.getAction())) {
			List<ServiceAuthenticator> auths = Utils
					.getAuthenticators(getBaseContext());

			for (ServiceAuthenticator auth : auths) {
				auth.onAccountsChanged(getBaseContext(), mCallback);
			}
		}
	}

	private AccountChangedCallback mCallback = new AccountChangedCallback() {
		@Override
		public void onAccountRemoved(Bundle bundle) {
			String token = Utils.getTokenString(bundle);
			if (token != null) {
				Log.i("magic", "hello! unlink with this token: " + token);
				// call unlink api here
			}
		}
	};

}
