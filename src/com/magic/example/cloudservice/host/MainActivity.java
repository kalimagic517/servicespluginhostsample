package com.magic.example.cloudservice.host;

import java.util.ArrayList;
import java.util.List;

import com.htc.cloudservice.common.ServiceAuthenticator;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends ListActivity {

	private Handler mWorkerHandler = null;
	private HandlerThread mHandlerThread = null;
	private MyAdapter mAdapter = null;

	private IntentFilter mPluginAccChangeFilter;

	private void initWorkerHandler() {
		mHandlerThread = new HandlerThread(MainActivity.class.getSimpleName());
		mHandlerThread.start();

		mWorkerHandler = new Handler(mHandlerThread.getLooper()) {

			@Override
			public void handleMessage(Message msg) {
				MainActivity.this.doHandleMessageInBg(msg);
				super.handleMessage(msg);
			}
		};
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initWorkerHandler();
		mAdapter = new MyAdapter(this);
		getListView().setAdapter(mAdapter);

		mPluginAccChangeFilter = new IntentFilter();
		mPluginAccChangeFilter
				.addAction(ServiceAuthenticator.ACTION_ACCOUNT_CHANGED);

	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(mPluginAccChangeReceiver);
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(mPluginAccChangeReceiver, mPluginAccChangeFilter);
		mWorkerHandler.obtainMessage(MSG_BUILD_ITEM_LIST).sendToTarget();
	}

	private BroadcastReceiver mPluginAccChangeReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			mWorkerHandler.obtainMessage(MSG_BUILD_ITEM_LIST).sendToTarget();
		}
	};

	@Override
	public void onDestroy() {
		if (mHandlerThread != null) {
			mHandlerThread.quit();
		}
		super.onDestroy();
	}

	private List<MyListItem> getDataList() {
		ArrayList<MyListItem> items = new ArrayList<MyListItem>();

		List<ServiceAuthenticator> auths = Utils.getAuthenticators(this);
		for (ServiceAuthenticator authenticator : auths) {
			MyListItem item = new MyListItem(this, authenticator);
			items.add(item);
		}
		return items;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		MyListItem item = mAdapter.getItem(position);
		Intent intent = item.getAuthenticator().getAuthIntent(
				getApplicationContext());
		if (intent != null) {
			startActivity(intent);
		}
	}

	private static final int MSG_BUILD_ITEM_LIST = 100;

	protected void doHandleMessageInBg(Message msg) {
		switch (msg.what) {
			case MSG_BUILD_ITEM_LIST : {
				final List<MyListItem> list = getDataList();
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						mAdapter.setData(list);
						mAdapter.notifyDataSetChanged();
					}
				});
				break;
			}
		}
	}

	private static class MyListItem {

		private String mLabel;
		private Bundle mAuthBundle;
		private ServiceAuthenticator mAuthticator;

		public MyListItem(Context context, ServiceAuthenticator authenticator) {
			mLabel = authenticator.getLabel(context);
			mAuthBundle = authenticator.getAuth(context);
			mAuthticator = authenticator;
		}

		public String getLabel() {
			return mLabel;
		}

		public String getTokenString() {
			return Utils.getTokenString(mAuthBundle);
		}

		public String getSharedSecret() {
			if (mAuthBundle != null) {
				if (mAuthBundle.containsKey("shared_secret")) {
					return mAuthBundle.getString("shared_secret");
				}
			}
			return null;
		}

		public ServiceAuthenticator getAuthenticator() {
			return mAuthticator;
		}
	}

	private static class MyAdapter extends BaseAdapter {

		private List<MyListItem> mDataList = null;
		private Context mContext = null;

		public MyAdapter(Context context) {
			mContext = context;
		}

		public void setData(List<MyListItem> list) {
			mDataList = list;
		}

		@Override
		public int getCount() {
			if (mDataList != null) {
				return mDataList.size();
			}
			return 0;
		}

		@Override
		public MyListItem getItem(int position) {
			if (mDataList != null) {
				return mDataList.get(position);
			}
			return null;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				view = View.inflate(mContext,
						android.R.layout.simple_list_item_2, null);
			}

			TextView text = (TextView) view.findViewById(android.R.id.text1);
			MyListItem item = mDataList.get(position);
			text.setText(item.getLabel());

			TextView text2 = (TextView) view.findViewById(android.R.id.text2);
			if (item.getTokenString() != null) {
				text2.setText(item.getTokenString());
				Log.e("magic", "name : " + item.getLabel());
				Log.e("magic", "token : " + item.getTokenString());
				Log.e("magic", "secret " + item.getSharedSecret());
			} else {
				text2.setText("");
			}
			return view;
		}
	}
}
