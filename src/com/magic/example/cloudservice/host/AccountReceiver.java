package com.magic.example.cloudservice.host;

import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AccountReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if (AccountManager.LOGIN_ACCOUNTS_CHANGED_ACTION.equals(intent
				.getAction())) {
			intent.setClass(context, AccountService.class);
			context.startService(intent);
		}
	}
}
